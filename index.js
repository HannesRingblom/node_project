const express = require("express")
const app = express()
const { PORT = 8080 } = process.env
const path = require("path")

//Middleware
app.use( express.static( path.join(__dirname, "public" )))

// Route
//http://localhost:8080
app.get("/", function(req, res){
    return res.sendFile(path.join(__dirname, "public", "index.html"))
})

//HTTP TRAFFIC
//GET, POST, PUT, PATCH, DELETE, OPTIONS

app.listen(PORT, function(){
    console.log("server has for sure started on port " + PORT)
})
