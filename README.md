# Cool web site

Runs a web-server on port 8080

## Table of Contents

- [Requirements](#requirements)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Maintainers](#maintainers)


## Requirements

Requires `Node.js`.

## Installation

```sh
#Install dependencies
$ npm install
```

## Usage

```sh
#Run the server
$ npm run dev
#Available on port:
$ http://localhost:8080
```

## Maintainers

[Hannes Ringblom @HannesRingblom](https://gitlab.com/HannesRingblom)
